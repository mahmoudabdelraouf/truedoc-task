<?php

namespace App\Imports;

use App\Client;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\Importable;

class ClientImport implements ToModel, WithChunkReading, ShouldQueue, WithValidation 
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $currentRowNumber = $this->getRowNumber();
        return new Client([
            //
            'first_name'=>$row[0],
            'second_name'=>$row[1],
            'family_name'=>$row[2],
            'uid'=>$row[3]
        ]);
    }
    public function chunkSize(): int
    {
        return 10;
    }
    public function rules(): array
    {
        return [
            '0' => 'required',
            '1' => 'required',
            '2' => 'required',
            '3' => 'required',
        ];
    }
}
