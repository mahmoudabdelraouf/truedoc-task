<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ImportClient;
use Validator;
use App\Client;
use App\Imports\ClientImport;
use Maatwebsite\Excel\Facades\Excel;
use Storage;

class ClientController extends Controller
{
    //

    public function import(ImportClient $request)
    {
        $file=$request->file;
        $filename= $file->getClientOriginalName();
        Storage::disk('public') -> put($filename, file_get_contents($file -> getRealPath()));
        $import=new ClientImport;
        try {
            Excel::queueImport($import,request()->file('file'));
            return "d";
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
             $failures = $e->failures();
             dd($failures);
             foreach ($failures as $failure) {
                 $failure->row(); // row that went wrong
                 $failure->attribute(); // either heading key (if using heading row concern) or column index
                 $failure->errors(); // Actual error messages from Laravel validator
                 $failure->values(); // The values of the row that has failed.
             }
        }
    }
}
